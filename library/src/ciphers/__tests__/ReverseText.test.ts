import {expect} from 'chai';
import ReverseText from '../ReverseText';

describe("ciphers/ReverseText", () => {
    describe("run", () => {
        it("should return a reversed string", () => {
            const cipher = new ReverseText();
            expect(cipher.run("reverse me please")).to.equal("esaelp em esrever");
        });
    })
});
