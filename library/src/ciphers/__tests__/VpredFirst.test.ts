import {expect} from 'chai';
import VpredFirst from "../VpredFirst";

describe("ciphers/VpredFirst", () => {
    describe("run", () => {
        it("should pad the strings correctly", () => {
            const cipher = new VpredFirst();
            expect(cipher.run("a", {
                blockSize: 3,
                paddingChars: ["a"]
            })).to.equal("aaaaaa");
            expect(cipher.run("bbb", {
                blockSize: 2,
                paddingChars: ["b"]
            })).to.equal("bbbb");
            expect(cipher.run("dddddddd", {
                blockSize: 4,
                paddingChars: ["c"]
            })).to.equal("dddddddd");
        });

        it("should encode an unpadded message correctly", () => {
            const cipher = new VpredFirst();
            expect(cipher.run("abcdefghIJKLMNOP", {
                blockSize: 4
            })).to.equal("acegbdfhIKMOJLNP")
        })

        it("should encode a padded message correctly", () => {
            const cipher = new VpredFirst();
            expect(cipher.run("abcdefghIJKL", {
                blockSize: 4
            })).to.equal("acegbdfhIKxxJLxx")
        })

        it("should respect the captitalize option", () => {
            const cipher = new VpredFirst();
            expect(cipher.run("abc", {
                blockSize: 2
            })).to.equal("acbx")
            expect(cipher.run("abc", {
                blockSize: 2,
                capitalize: true
            })).to.equal("ACBX")
        })
    })
});
