import {expect} from 'chai';
import Petronilka from '../Petronilka';

describe("ciphers/Petronilka", () => {
    describe("run", () => {
        it("should correctly encode the word PETRONILKA in both uppercase and lowercase", () => {
            const cipher = new Petronilka();
            expect(cipher.run("petronilka")).to.equal("1234567890");
            expect(cipher.run("PETRONILKA")).to.equal("1234567890");
        });
        it("should correctly strip diacritics and encode them as their base letters", () => {
            const cipher = new Petronilka();
            expect(cipher.run("ĚŠČŘŽÝÁÍÉĎŤŇÚŮ")).to.equal(cipher.run("ESCRZYAIEDTNUU"));
            expect(cipher.run("ěščřžýáíéďťňúů")).to.equal(cipher.run("escrzyaiedtnuu"));
        });
        it("should correctly not encode other symbols, than the ones in PETRONILKA", () => {
            const cipher = new Petronilka();
            expect(cipher.run("xyzpetronilka xyz")).to.equal("xyz1234567890 xyz");
        });
    })
});
