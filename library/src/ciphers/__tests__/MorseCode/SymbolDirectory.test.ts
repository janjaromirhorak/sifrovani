import {expect} from 'chai';
import {MorseSymbol, SymbolDirectory} from '../../MorseCode';

describe("ciphers/MorseCode/SymbolDirectory", () => {
    describe("run", () => {
        it("should return characters from directory", () => {
            const directory = new SymbolDirectory({});
            expect(directory.getCharacter([MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT])).to.equal("d");
        });
    })
});
