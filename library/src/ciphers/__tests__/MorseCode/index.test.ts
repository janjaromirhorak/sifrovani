import {expect} from 'chai';
import {parameterDefinitions} from '../../MorseCode';

describe("ciphers/MorseCode/index", () => {
    describe("run", () => {
        it("should export parameter definitions", () => {
            expect(parameterDefinitions.dashSymbol.defaultValue).to.equal("-");
        });
    })
});
