import {expect} from 'chai';
import {Spider} from "../../MorseCode";

describe("ciphers/MorseCode/Spider", () => {
    describe("getRowsData", () => {
        it("should return a correct first row", () => {
            const spider = new Spider({});
            const {left, right} = spider.getRowsData();
            expect(left[0].length).to.equal(1);
            expect(right[0].length).to.equal(1);

            const [[e]] = left;
            const [[t]] = right;

            expect(e.symbols).to.equal(".");
            expect(t.symbols).to.equal("-");

            expect(e.character).to.equal("e");
            expect(t.character).to.equal("t");
        });

        it("should respect custom symbol configuration", () => {
            const spider = new Spider({
                dotSymbol: "DOT",
                dashSymbol: "DASH"
            });
            const {left, right} = spider.getRowsData();
            expect(left[0].length).to.equal(1);
            expect(right[0].length).to.equal(1);

            const [[e]] = left;
            const [[t]] = right;

            expect(e.symbols).to.equal("DOT");
            expect(t.symbols).to.equal("DASH");

            expect(e.character).to.equal("e");
            expect(t.character).to.equal("t");
        });
    })
});
