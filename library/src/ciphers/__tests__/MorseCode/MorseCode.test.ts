import {expect} from 'chai';
import MorseCode, {MorseSymbol} from '../../MorseCode';

describe("ciphers/MorseCode/MorseCode", () => {
    describe("run", () => {
        it("should correctly handle an empty row", () => {
            const cipher = new MorseCode();
            expect(cipher.run("a\n\nb")).to.equal(".-||\n\n-...||")
        });

        it("should correctly encode the alphabet", () => {
            const cipher = new MorseCode();
            expect(cipher.run("abcdefghijklmnopqrstuvwxyz")).to.equal(".-|-...|-.-.|-..|.|..-.|--.|....|..|.---|-.-|.-..|--|-.|---|.--.|--.-|.-.|...|-|..-|...-|.--|-..-|-.--|--..||");
        });

        it("should encode ch as a single character, if specified", () => {
            const cipher = new MorseCode();
            expect(cipher.run("ch", {
                useCh: true
            })).to.equal("----||");
        });

        it("should encode ch as two characters, if specified", () => {
            const cipher = new MorseCode();
            expect(cipher.run("ch", {
                useCh: false
            })).to.equal("-.-.|....||");
        });

        it("should correctly separate words", () => {
            const cipher = new MorseCode();
            expect(cipher.run("ab cd e", {
                useCh: false
            })).to.equal(".-|-...||-.-.|-..||.||");
        });

        it("should correctly encode numbers, when useNumbers is true", () => {
            const cipher = new MorseCode();
            expect(cipher.run("0123456789", {
                useNumbers: true
            })).to.equal("-----|.----|..---|...--|....-|.....|-....|--...|---..|----.||");
        });

        it("should not encode numbers, when useNumbers is false", () => {
            const cipher = new MorseCode();
            expect(cipher.run("0123456789", {
                useNumbers: false,
                removeUnnecessaryPunctuation: false
            })).to.equal("0|1|2|3|4|5|6|7|8|9||");
        });

        it("should correctly encode symbols, when useSpecialSymbols is true", () => {
            const cipher = new MorseCode();
            expect(cipher.run(".,?'!/()&:;=+-_\"$@", {
                useSpecialSymbols: true,
                removeUnnecessaryPunctuation: false
            })).to.equal(".-.-.-|--..--|..--..|.----.|-.-.--|-..-.|-.--.|-.--.-|.-...|---...|-.-.-.|-...-|.-.-.|-....-|..--.-|.-..-.|...-..-|.--.-.||");
        });

        it("should not encode symbols, when useSpecialSymbols is  false", () => {
            const cipher = new MorseCode();
            expect(cipher.run(".,?'!/()&:;=+-_\"$@", {
                useSpecialSymbols: false,
                removeUnnecessaryPunctuation: false
            })).to.equal("|,||'||/|(|)|&||;|=|+|-|_|\"|$|@||");
        });

        it("should not encode obscure characters", () => {
            const cipher = new MorseCode();
            expect(cipher.run("a🤖c", {
                useSpecialSymbols: true,
                removeUnnecessaryPunctuation: false
            })).to.equal(".-|🤖|-.-.||");
        });

        it("should remove diacriticts by default", () => {
            const cipher = new MorseCode();
            expect(cipher.run("ěščřžýáíéďťňúů")).to.equal(".|...|-.-.|.-.|--..|-.--|.-|..|.|-..|-|-.|..-|..-||");
        });

        it("should not remove diacriticts, when the removeDiacritics option is false", () => {
            const cipher = new MorseCode();
            expect(cipher.run("ěščřžýáíéďťňúů", {
                removeDiacritics: false
            })).to.equal("ě|š|č|ř|ž|ý|á|í|é|ď|ť|ň|ú|ů||");
        });

        it("should respect symbol overrides", () => {
            const cipher = new MorseCode();
            expect(cipher.run("aaa bb c", {
                dotSymbol: "@",
                dashSymbol: "#",
                separatorSymbol: "%"
            })).to.equal("@#%@#%@#%%#@@@%#@@@%%#@#@%%");
        });
    });

    describe("getSymbolMapForConfig", () => {
        it("should return the correct symbol map for the default configuration (without CH)", () => {
            const cipher = new MorseCode();
            const map = cipher.getSymbolMapForConfig({
                useCh: false
            });
            expect(map.get("a")).to.have.ordered.members([MorseSymbol.DOT, MorseSymbol.DASH]);
            expect(map.get("ch")).to.equal(undefined);
        });

        it("should return the correct symbol map for the default configuration (with CH)", () => {
            const cipher = new MorseCode();
            const map = cipher.getSymbolMapForConfig({
                useCh: true
            });
            expect(map.get("a")).to.have.ordered.members([MorseSymbol.DOT, MorseSymbol.DASH]);
            expect(map.get("ch")).to.have.ordered.members([MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        });

        it("should return the correct symbol map for a configuration with numbers", () => {
            const cipher = new MorseCode();
            const map = cipher.getSymbolMapForConfig({
                useNumbers: true
            });
            expect(map.get("a")).to.have.ordered.members([MorseSymbol.DOT, MorseSymbol.DASH]);
            expect(map.get("8")).to.have.ordered.members([MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);
        });

        it("should return the correct symbol map for a configuration with special symbols", () => {
            const cipher = new MorseCode();
            const map = cipher.getSymbolMapForConfig({
                useSpecialSymbols: true
            });
            expect(map.get("a")).to.have.ordered.members([MorseSymbol.DOT, MorseSymbol.DASH]);
            expect(map.get("_")).to.have.ordered.members([MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        });
    });

    describe("getSymbolMapsForConfig", () => {
        it("should return the correct number of tables for config without numbers and special symbols", () => {
            const cipher = new MorseCode();
            const maps = cipher.getSymbolMapsForConfig({
                useNumbers: false,
                useSpecialSymbols: false
            });
            expect(maps.length).to.equal(1);
        });

        it("should return the correct number of tables for config with numbers and without special symbols", () => {
            const cipher = new MorseCode();
            const maps = cipher.getSymbolMapsForConfig({
                useNumbers: true,
                useSpecialSymbols: false
            });
            expect(maps.length).to.equal(2);
        });

        it("should return the correct number of tables for config without numbers and with special symbols", () => {
            const cipher = new MorseCode();
            const maps = cipher.getSymbolMapsForConfig({
                useNumbers: false,
                useSpecialSymbols: true
            });
            expect(maps.length).to.equal(2);
        });

        it("should return the correct number of tables for config with numbers and with special symbols", () => {
            const cipher = new MorseCode();
            const maps = cipher.getSymbolMapsForConfig({
                useNumbers: true,
                useSpecialSymbols: true
            });
            expect(maps.length).to.equal(3);
        });
    })
});
