import {expect} from 'chai';
import VpredSecond from '../VpredSecond';

describe("ciphers/VpredSecond", () => {
    describe("run", () => {
        it("should work with a 2x2 square", () => {
            const cipher = new VpredSecond();
            expect(cipher.run("0123")).to.equal("0132");
        });
        it("should work with a 3x3 square", () => {
            const cipher = new VpredSecond();
            expect(cipher.run("012345678")).to.equal("041785362");
        });
        it("should work with a 4x4 square", () => {
            const cipher = new VpredSecond();
            expect(cipher.run("0123456789ABCDEF")).to.equal("0481BCD57FE93A62");
        });
        it("should work with a 5x5 square", () => {
            const cipher = new VpredSecond();
            expect(cipher.run("0123456789ABCDEFGHIJKLMNO")).to.equal("048C1FGKH5BNOL97JMID3EA62");
        });
        it("padding should work correctly", () => {
            const cipher = new VpredSecond();
            const fullString = "0123456789ABCDEFGHIJKLMNO";
            for (let i = Math.pow((Math.sqrt(fullString.length) - 1), 2) + 1 ; i < fullString.length ; i++) {
                const shortString = fullString.substring(0, i);
                const paddedString = shortString + "X".repeat(fullString.length - i);
                expect(cipher.run(shortString)).to.equal(cipher.run(paddedString));
            }
        });
    })
});
