import {expect} from 'chai';
import ArgumentError from '../../core/ArgumentError';
import CharacterAverages from '../CharacterAverages';

describe("ciphers/CharacterAverages", () => {
    describe("run", () => {
        it("should work consistently with the default parameters", () => {
            const cipher = new CharacterAverages();
            expect(cipher.run("abcd efgh ijkl mnop qrst uvwx yz")).to.equal("ZB AC BD CE | DF EG FH GI | HJ IK JL KM | LN MO NP OQ | PR QS RT SU | TV UW VX WY | XZ YA");
        });

        it("should work consistently with custom range", () => {
            const cipher = new CharacterAverages();
            expect(cipher.run("a", {
                minDistance: 5,
                maxDistance: 7
            })).to.satisfy((ciphertext: string) => {
                return ["VF", "UG", "TH"].includes(ciphertext);
            })
        });

        it("should not map non-alphabetic characters", () => {
            const cipher = new CharacterAverages();
            expect(cipher.run("a?")).to.equal("ZB ?");
        });

        it("should work consistently with a custom alphabet", () => {
            const cipher = new CharacterAverages();
            expect(cipher.run("a079b", {
                alphabet: "0123456789".split("")
            })).to.equal("a 91 68 80 b");
        });

        it("should throw an exception, when provided with an invalid 'minDistance' value", () => {
            const cipher = new CharacterAverages();
            expect(() => {
                cipher.run("", {
                    minDistance: 1/0
                })
            }).to.throw(ArgumentError);
        });

        it("should throw an exception, when provided with an invalid 'maxDistance' value", () => {
            const cipher = new CharacterAverages();
            expect(() => {
                cipher.run("", {
                    maxDistance: 1/0
                })
            }).to.throw(ArgumentError);
        });

        it("should throw an exception, when provided with a negative 'minDistance' value", () => {
            const cipher = new CharacterAverages();
            expect(() => {
                cipher.run("", {
                    minDistance: -15
                })
            }).to.throw(ArgumentError);
        });

        it("should throw an exception, when provided with 'minDistance' greater than 'maxDistance'", () => {
            const cipher = new CharacterAverages();
            expect(() => {
                cipher.run("", {
                    minDistance: 15,
                    maxDistance: 5
                })
            }).to.throw(ArgumentError);
        });

        it("should throw an exception, when provided with a blank alphabet", () => {
            const cipher = new CharacterAverages();
            expect(() => {
                cipher.run("", {
                    alphabet: []
                })
            }).to.throw(ArgumentError);
        })
    })
});
