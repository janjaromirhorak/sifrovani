import {expect} from 'chai';
import FirstLast from '../FirstLast';

describe("ciphers/FirstLast", () => {
    describe("run", () => {
        it("should return the correct ciphertext, when tested with odd number of characters", () => {
            const cipher = new FirstLast();
            expect(cipher.run("first to last")).to.equal("frtoatsltsi");
        });


        it("should return the correct ciphertext, when tested with even number of characters", () => {
            const cipher = new FirstLast();
            expect(cipher.run("first to last!")).to.equal("frtoat!sltsi");
        });
    })
});
