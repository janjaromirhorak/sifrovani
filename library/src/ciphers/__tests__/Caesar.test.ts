import {expect} from 'chai';
import Caesar from '../Caesar';

describe("ciphers/Caesar", () => {
    describe("run", () => {
        it("with shift 0, should return cleartext", () => {
            const cipher = new Caesar();
            expect(cipher.run("abcdefghijklmnopqrstuvwxyz", {
                shift: 0
            })).to.equal("abcdefghijklmnopqrstuvwxyz");
        });

        it("should return the correct ciphertext for shift 1", () => {
            const cipher = new Caesar();
            expect(cipher.run("abcdefghijklmnopqrstuvwxyz", {
                shift: 1
            })).to.equal("bcdefghijklmnopqrstuvwxyza");
        });

        it("should return the correct ciphertext for shift -1", () => {
            const cipher = new Caesar();
            expect(cipher.run("abcdefghijklmnopqrstuvwxyz", {
                shift: -1
            })).to.equal("zabcdefghijklmnopqrstuvwxy");
        });

        it("should preserve case", () => {
            const cipher = new Caesar();
            expect(cipher.run("aABb", {
                shift: 5
            })).to.equal("fFGg");
        });

        it("should shift only characters from the alphabet", () => {
            const cipher = new Caesar();
            expect(cipher.run("abc' %???q", {
                shift: -1
            })).to.equal("zab' %???p");
        });

        it("should support shifting by numbers greater than 26", () => {
            const cipher = new Caesar();
            expect(cipher.run("xyzabc", {
                shift: 2601
            })).to.equal("yzabcd");
        });

        it("should support shifting by numbers smaller than -26", () => {
            const cipher = new Caesar();
            expect(cipher.run("xyzabc", {
                shift: -2625
            })).to.equal("yzabcd");
        });
    })
});
