import {expect} from 'chai';
import ReverseWords from '../ReverseWords';

describe("ciphers/ReverseWords", () => {
    describe("run", () => {
        it("should return a string with reversed words", () => {
            const cipher = new ReverseWords();
            expect(cipher.run("reverse my words")).to.equal("esrever ym sdrow");
        });
    })
});
