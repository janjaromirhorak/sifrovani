import {Cipher} from "../core";
import ArgumentError from "../core/ArgumentError";
import {arraysAreSame, getRandomInt, removeDiacritics} from "../core/utils";
import {ParameterDefinitions, CipherFunctionType} from "../core/Cipher";

export type Parameters = {
    minDistance?: number,
    maxDistance?: number,
    alphabet?: string[],
    separator?: string
}

const defaultAlphabet: string[] = "abcdefghijklmnopqrstuvwxyz".split("");

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
    minDistance: {
        name: "Minimum distance",
        description: "Minimum distance from the cleartext letter. Must be greater than or equal to 0.",
        defaultValue: 1
    },
    maxDistance: {
        "name": "Maximum distance",
        "description": "Maximum distance from the cleartext letter. Must be greater than or equal to minDistance.",
        defaultValue: 1
    },
    alphabet: {
        name: "Alphabet",
        description: "An alphabet to be used. By default the standard 26-char english alphabet is used.",
        defaultValue: defaultAlphabet
    },
    separator: {
        name: "Separator",
        description: "A string used for separating words.",
        defaultValue: " | "
    }
};

const cipherFunction: CipherFunctionType<Parameters> = ((cleartext, {minDistance, maxDistance, alphabet, separator}) => {
    // GET AND VALIDATE THE PARAMETERS

    if (!isFinite(minDistance)) {
        throw new ArgumentError("The argument 'minDistance' must be an integer.");
    }

    if (!isFinite(maxDistance)) {
        throw new ArgumentError("The argument 'maxDistance' must be an integer.");
    }

    if (minDistance < 0) {
        throw new ArgumentError("The argument 'minDistance' must be 0 or greater.");
    }

    if (maxDistance < minDistance) {
        throw new ArgumentError(`The argument 'maxDistance' must be greater than or equal to 'minDistance' (${minDistance}).`)
    }

    if (alphabet.length === 0) {
        throw new ArgumentError("The argument 'alphabet' is an empty string. If you wish to use the default alphabet, please omit this argument.");
    }

    // GENERATE A SUBSTITUTION MAP

    let substitutionMap = new Map();
    for (let idx = 0; idx < alphabet.length; idx++) {
        const char = alphabet[idx];
        for (let i = minDistance; i <= maxDistance; i++) {
            const leftIndex = (idx + alphabet.length - i) % alphabet.length;
            const rightIndex = (idx + i) % alphabet.length;
            const leftChar = alphabet[leftIndex];
            const rightChar = alphabet[rightIndex];

            const charPair = `${leftChar}${rightChar}`.toUpperCase();

            if (substitutionMap.has(char)) {
                substitutionMap.set(char, [...substitutionMap.get(char), charPair]);
            } else {
                substitutionMap.set(char, [charPair]);
            }
        }
    }

    // PREPARE THE INPUT

    let cleantext = cleartext;

    // if the default alphabet is used, we know how to sanitize the cleartext
    if (arraysAreSame(alphabet, defaultAlphabet)) {
        cleantext = removeDiacritics(cleantext).toLowerCase();
    }

    // PERFORM THE SUBSTITION

    // split to words
    const words = cleantext.split(' ').filter(word => word.length);

    // perform the substitution on each word
    const cipherWords = words.map(word =>
        word
            .split('')
            .map(char => {
                if (substitutionMap.has(char)) {
                    const variants = substitutionMap.get(char);
                    return variants[getRandomInt(0, variants.length - 1)];
                }

                return char;
            })
            .join(' ')
    );

    // join the words together
    return cipherWords.join(separator);
});

class CharacterAverages extends Cipher<Parameters> {
    constructor() {
        super(parameterDefinitions, cipherFunction);
    }
}

export default CharacterAverages;