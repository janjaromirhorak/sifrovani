import { Cipher } from '../core';
import ArgumentError from '../core/ArgumentError';
import { ParameterDefinitions } from '../core/Cipher';
import { pad, removeDiacritics, splitToChunks, unzip, zip } from '../core/utils';

export type Parameters = {
	keywordX: string;
	keywordY: string;
	separator: string;
};

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
	keywordX: {
		name: 'Horizontal keyword',
		description: 'Specifies the keyword used on the horizontal axis',
		defaultValue: 'ABCDE'
	},
	keywordY: {
		name: 'Vertical keyword',
		description: 'Specifies the keyword used on the vertical axis',
		defaultValue: 'ABCDE'
	},
	separator: {
		name: 'Character separator',
		description: 'Character used to separate the character pairs.',
		defaultValue: '|'
	}
};

class Coordinates extends Cipher<Parameters> {
	constructor() {
		super(parameterDefinitions, (cleartext, { keywordX, keywordY, separator }) => {
			const charsX = keywordX.split('');
			const charsY = keywordY.split('');

			if (charsX.length !== 5) {
				throw new ArgumentError("The argument 'keywordX' must have a length of 5.");
			}

			if (charsY.length !== 5) {
				throw new ArgumentError("The argument 'keywordY' must have a length of 5.");
			}

			// prepare the encoding key
			const alphabet = 'abcdefghijklmnoprstuvwxyz'.split(''); // intentionally no Q
			let key = new Map<string, string>();
			let index = 0;
			for (const y of charsY) {
				for (const x of charsX) {
					key.set(alphabet[index], `${y}${x}`);
					index++;
				}
			}

			// remove whitespace and split
			return removeDiacritics(cleartext)
				.replace(/\s/g, '')
				.split('')
				.map((char) => key.get(char.toLowerCase()) ?? char)
				.join(separator);
		});
	}
}

export default Coordinates;
