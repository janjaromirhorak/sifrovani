import { Cipher } from "../core";
import {CipherFunctionType} from "../core/Cipher";

export type Parameters = {};

const cipherFunction: CipherFunctionType<Parameters> = cleartext => {
    return [...cleartext.split('')].reverse().join('');
};

class ReverseText extends Cipher<Parameters> {
    constructor() {
        super({}, cipherFunction);
    }
}

export default ReverseText;