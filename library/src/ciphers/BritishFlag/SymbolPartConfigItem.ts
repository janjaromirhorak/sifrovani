interface SymbolPartConfigItem {
    path: string,
    squareEnd?: boolean
}

export default SymbolPartConfigItem;