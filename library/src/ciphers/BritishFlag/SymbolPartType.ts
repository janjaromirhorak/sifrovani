enum SymbolPartType {
    TopRowLeft,
    TopRowRight,
    MiddleRowLeft,
    MiddleRowRight,
    BottomRowLeft,
    BottomRowRight,
    LeftColumnTop,
    LeftColumnBottom,
    MiddleColumnTop,
    MiddleColumnBottom,
    RightColumnTop,
    RightColumnBottom,
    DiagonalLeftTop,
    DiagonalRightTop,
    DiagonalLeftBottom,
    DiagonalRightBottom
}

export default SymbolPartType;