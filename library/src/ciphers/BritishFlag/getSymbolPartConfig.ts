import SymbolPartType from "./SymbolPartType";
import SymbolPartConfigItem from "./SymbolPartConfigItem";

const getSymbolPartConfig = (): Map<SymbolPartType, SymbolPartConfigItem> => {
    const symbolPartConfig = new Map<SymbolPartType, SymbolPartConfigItem>();
    symbolPartConfig.set(SymbolPartType.TopRowLeft, {
        path: "m21 14h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.TopRowRight, {
        path: "m26 14h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.MiddleRowLeft, {
        path: "m21 19h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.MiddleRowRight, {
        path: "m26 19h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.BottomRowLeft, {
        path: "m21 24h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.BottomRowRight, {
        path: "m26 24h5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.LeftColumnTop, {
        path: "m21 19v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.LeftColumnBottom, {
        path: "m21 24v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.MiddleColumnTop, {
        path: "m26 19v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.MiddleColumnBottom, {
        path: "m26 24v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.RightColumnTop, {
        path: "m31 19v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.RightColumnBottom, {
        path: "m31 24v-5",
        squareEnd: true
    });
    symbolPartConfig.set(SymbolPartType.DiagonalLeftTop, {
        path: "m26 19-5-5"
    });
    symbolPartConfig.set(SymbolPartType.DiagonalRightTop, {
        path: "m26 19 5-5"
    });
    symbolPartConfig.set(SymbolPartType.DiagonalLeftBottom, {
        path: "m21 24 5-5"
    });
    symbolPartConfig.set(SymbolPartType.DiagonalRightBottom, {
        path: "m31 24-5-5"
    });
    return symbolPartConfig;
};

export default getSymbolPartConfig;