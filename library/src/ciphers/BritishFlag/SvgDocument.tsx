import React from "react";
import Symbol from "./Symbol";
import getSymbolPartConfig from "./getSymbolPartConfig";
import getCharacterConfig from "./getCharacterConfig";

interface Props {
    text: string,
    charsPerRow: number
}

const startPoint = -0.25;
const charSize = 10.75;
const charSpacing = 2;


const SvgDocument = ({text, charsPerRow}: Props) => {
    const symbolPartConfig = getSymbolPartConfig();
    const characterConfig = getCharacterConfig();

    const rowCount = Math.ceil(text.length / charsPerRow);
    const colCount = Math.min(text.length, charsPerRow);

    const width = (charSize * colCount) + ((colCount - 1) * charSpacing);
    const height = (charSize * rowCount) + ((rowCount - 1) * charSpacing);

    const documentWidth = width;
    const documentHeight = height;

    const viewBoxHorizontalMax = startPoint + width;
    const viewBoxVerticalMax = startPoint + height;

    return (
        <svg width={`${documentWidth}mm`} height={`${documentHeight}mm`}
             version="1.1"
             viewBox={`${startPoint} ${startPoint} ${viewBoxHorizontalMax} ${viewBoxVerticalMax}`}
             xmlns="http://www.w3.org/2000/svg">
            {text.split("").map((character, key) => {
                    const rowIndex = Math.floor(key / charsPerRow);
                    const colIndex = key % charsPerRow;

                    return (
                        <Symbol
                            key={key}
                            character={character}
                            symbolPartConfig={symbolPartConfig}
                            characterConfig={characterConfig}
                            horizontalOffset={colIndex * (charSize + charSpacing)}
                            verticalOffset={rowIndex * (charSize + charSpacing)}
                        />
                    )
                }
            )}
        </svg>
    );
};

export default SvgDocument;