import BritishFlag from "./BritishFlag";
import type {Parameters} from "./BritishFlag";
import {parameterDefinitions} from "./BritishFlag";

export default BritishFlag;

export type {
    Parameters
};

export {
    parameterDefinitions
}