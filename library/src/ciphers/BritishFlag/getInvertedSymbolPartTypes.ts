import SymbolPartType from "./SymbolPartType";

const getInvertedSymbolPartTypes = (types: SymbolPartType[]): SymbolPartType[] => {
    return [
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.DiagonalLeftBottom,
        SymbolPartType.DiagonalRightBottom
    ].filter(type => !types.includes(type));
};

export default getInvertedSymbolPartTypes;