import ReactDOMServer from 'react-dom/server';
import {Cipher} from "../../core";
import {CipherFunctionType, ParameterDefinitions} from "../../core/Cipher";
import SvgDocument from "./SvgDocument";
import {removeDiacritics, removeNonAlphaNumericCharacters} from "../../core/utils";

export type Parameters = {
    charsPerRow: number
}

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
    charsPerRow: {
        name: "Characters per row",
        description: "Number of characters to be rendered one one row",
        defaultValue: 15
    }
};

const cipherFunction: CipherFunctionType<Parameters> = (cleartext, parameters) => {
    return ReactDOMServer.renderToStaticMarkup(SvgDocument({
        text: removeNonAlphaNumericCharacters(removeDiacritics(cleartext)).toLocaleLowerCase(),
        charsPerRow: parameters.charsPerRow
    }));
};

class BritishFlag extends Cipher<Parameters> {
    constructor() {
        super(parameterDefinitions, cipherFunction);
    }
}

export default BritishFlag;