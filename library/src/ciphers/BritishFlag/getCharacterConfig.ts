import SymbolPartType from "./SymbolPartType";
import getInvertedSymbolPartTypes from "./getInvertedSymbolPartTypes";

const getCharacterConfig = (): Map<string, SymbolPartType[]> => {
    const characterConfig = new Map<string, SymbolPartType[]>();
    characterConfig.set("a", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.MiddleRowRight
    ]))
    characterConfig.set("b", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalLeftBottom,
        SymbolPartType.MiddleRowLeft
    ]))
    characterConfig.set("c", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("d", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalLeftBottom
    ]))
    characterConfig.set("e", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("f", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleRowLeft
    ]))
    characterConfig.set("g", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.MiddleRowRight
    ]))
    characterConfig.set("h", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.MiddleRowRight
    ]))
    characterConfig.set("i", getInvertedSymbolPartTypes([
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom
    ]))
    characterConfig.set("j", getInvertedSymbolPartTypes([
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.DiagonalRightBottom
    ]))
    characterConfig.set("k", getInvertedSymbolPartTypes([
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.DiagonalRightBottom
    ]))
    characterConfig.set("l", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.BottomRowLeft
    ]))
    characterConfig.set("m", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop,
    ]))
    characterConfig.set("n", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightBottom,
    ]))
    characterConfig.set("o", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("p", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.TopRowLeft,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleRowLeft
    ]))
    characterConfig.set("q", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight,
        SymbolPartType.DiagonalRightBottom
    ]))
    characterConfig.set("r", getInvertedSymbolPartTypes([
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.TopRowRight,
        SymbolPartType.RightColumnTop,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.DiagonalRightBottom
    ]))
    characterConfig.set("s", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.TopRowLeft,
        SymbolPartType.LeftColumnTop,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.BottomRowRight,
        SymbolPartType.BottomRowLeft
    ]))
    characterConfig.set("t", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.TopRowLeft,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom
    ]))
    characterConfig.set("u", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.LeftColumnBottom,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom
    ]))
    characterConfig.set("v", getInvertedSymbolPartTypes([
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop
    ]))
    characterConfig.set("w", getInvertedSymbolPartTypes([
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.MiddleColumnTop
    ]))
    characterConfig.set("x", getInvertedSymbolPartTypes([
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.DiagonalLeftBottom,
        SymbolPartType.DiagonalRightBottom
    ]))
    characterConfig.set("y", getInvertedSymbolPartTypes([
        SymbolPartType.DiagonalLeftTop,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.MiddleColumnBottom
    ]))
    characterConfig.set("z", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowLeft,
        SymbolPartType.TopRowRight,
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.DiagonalLeftBottom,
        SymbolPartType.BottomRowLeft,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("0", getInvertedSymbolPartTypes([
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("1", getInvertedSymbolPartTypes([
        SymbolPartType.DiagonalRightTop,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom
    ]))
    characterConfig.set("2", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowLeft,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.DiagonalLeftBottom,
        SymbolPartType.BottomRowLeft
    ]))
    characterConfig.set("3", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.BottomRowRight,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom
    ]))
    characterConfig.set("4", getInvertedSymbolPartTypes([
        SymbolPartType.LeftColumnTop,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleRowLeft,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.MiddleColumnBottom
    ]))
    characterConfig.set("5", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("6", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.MiddleRowRight,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.BottomRowRight
    ]))
    characterConfig.set("7", getInvertedSymbolPartTypes([
        SymbolPartType.TopRowRight,
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom
    ]))
    characterConfig.set("8", getInvertedSymbolPartTypes([
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.MiddleColumnBottom,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowRight,
        SymbolPartType.MiddleRowRight
    ]))
    characterConfig.set("9", getInvertedSymbolPartTypes([
        SymbolPartType.RightColumnTop,
        SymbolPartType.RightColumnBottom,
        SymbolPartType.MiddleColumnTop,
        SymbolPartType.TopRowRight,
        SymbolPartType.BottomRowRight,
        SymbolPartType.MiddleRowRight
    ]))
    return characterConfig;
};

export default getCharacterConfig;