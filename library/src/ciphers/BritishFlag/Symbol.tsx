import React from "react";
import SymbolPartType from "./SymbolPartType";
import SymbolPartConfigItem from "./SymbolPartConfigItem";

interface Props {
    character: string,
    symbolPartConfig: Map<SymbolPartType, SymbolPartConfigItem>,
    characterConfig: Map<string, SymbolPartType[]>,
    verticalOffset?: number,
    horizontalOffset?: number
}

const Symbol = ({character, symbolPartConfig, characterConfig, verticalOffset = 0, horizontalOffset = 0}: Props) => {
    const symbols: SymbolPartType[] = characterConfig.get(character) ?? [];

    return (
        <g transform={`translate(${-21 + horizontalOffset} ${-14 + verticalOffset})`} fill="none" stroke="#000" strokeWidth=".5">
            {symbols.map(type => {
                const config = symbolPartConfig.get(type);
                if (config) {
                    return <path d={config.path} strokeLinecap={config.squareEnd ? "square" : "butt"}/>
                }
                return null;
            })}
        </g>
    );
}

export default Symbol;