import { Cipher } from '../core';
import { CipherFunctionType, ParameterDefinitions } from '../core/Cipher';
import { removeDiacritics, removeWhiteSpaces } from '../core/utils';

export type Parameters = {
	removeDiacritics?: boolean;
	removePunctuation?: boolean;
	capitalize?: boolean;
};

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
	removeDiacritics: {
		name: 'Remove diacritics',
		description: 'Whether or not should diacritics be removed',
		defaultValue: true
	},
	removePunctuation: {
		name: 'Remove punctuation',
		description: 'Whether or not should punctuation be removed',
		defaultValue: false
	},
	capitalize: {
		name: 'Capitalize',
		description: 'Whether or not should the message be capitalized',
		defaultValue: true
	}
};

const cipherFunction: CipherFunctionType<Parameters> = (cleartext, parameters) => {
	let preformattedCleartext = cleartext;

	if (parameters.removeDiacritics) {
		preformattedCleartext = removeDiacritics(preformattedCleartext);
	}

	if (parameters.removePunctuation) {
		preformattedCleartext = preformattedCleartext.replace(/[,\.!?-]/g, '');
	}

	if (parameters.capitalize) {
		preformattedCleartext = preformattedCleartext.toUpperCase();
	}

	return preformattedCleartext
		.split(' ')
		.filter((word) => word.length)
		.map((word) => [...word.split('')].reverse().join(''))
		.join(' ');
};

class ReverseText extends Cipher<Parameters> {
	constructor() {
		super(parameterDefinitions, cipherFunction);
	}
}

export default ReverseText;
