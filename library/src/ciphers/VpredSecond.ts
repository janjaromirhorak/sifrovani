import {Cipher} from "../core";
import {removeWhiteSpaces} from "../core/utils";
import {CipherFunctionType} from "../core/Cipher";

export type Parameters = {}

const cipherFunction: CipherFunctionType<Parameters> = (cleartext) => {
    const charArray = removeWhiteSpaces(cleartext).toLocaleUpperCase().split('');

    // find the smallest square, that fits all the characters
    const squareSize = Math.ceil(Math.sqrt(charArray.length));

    // create the output table and fill it with the padding characters
    let finalTable = Array(squareSize * squareSize).fill("X");
    let visitedLocations = new Set();

    let rowPointer = 0;
    let colPointer = -1;
    let step = 0;

    for (const char of charArray) {
        if (step === 0) {
            colPointer++;

            if (visitedLocations.has(rowPointer * squareSize + colPointer)) {
                rowPointer++;
                colPointer = rowPointer;
            }
        }

        visitedLocations.add(rowPointer * squareSize + colPointer);

        finalTable[rowPointer * squareSize + colPointer] = char;

        [rowPointer, colPointer] = [colPointer, squareSize - 1 - rowPointer];

        step = (step + 1) % 4;
    }

    return `${finalTable.join('')}`;
};

class VpredSecond extends Cipher<Parameters> {
    constructor() {
        super({}, cipherFunction);
    }
}

export default VpredSecond;