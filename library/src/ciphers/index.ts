import BritishFlag from "./BritishFlag"
import Caesar from "./Caesar";
import CharacterAverages from "./CharacterAverages";
import FirstLast from "./FirstLast";
import MorseCode from "./MorseCode";
import Petronilka from "./Petronilka";
import ReverseText from "./ReverseText"
import ReverseWords from "./ReverseWords"
import VpredFirst from "./VpredFirst";
import VpredSecond from "./VpredSecond";

export {
    BritishFlag,
    Caesar,
    CharacterAverages,
    FirstLast,
    MorseCode,
    Petronilka,
    ReverseText,
    ReverseWords,
    VpredFirst,
    VpredSecond
}