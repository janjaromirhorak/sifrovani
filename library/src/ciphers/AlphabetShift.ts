import { Cipher } from '../core';
import ArgumentError from '../core/ArgumentError';
import { ParameterDefinitions } from '../core/Cipher';
import { removeDiacritics } from '../core/utils';

export type Parameters = {
	keyword: string;
};

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
	keyword: {
		name: 'Keyword',
		description: 'Keyword used to shift the alphabet. Must have unique letters.',
		defaultValue: 'KEYWORD'
	}
};

class AlphabetShift extends Cipher<Parameters> {
	constructor() {
		super(parameterDefinitions, (cleartext, parameters) => {
			const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
			const shiftedAlphabet = this.getShiftedAlphabet(parameters);

			return removeDiacritics(cleartext)
				.toLowerCase()
				.split('')
				.map((letter) => shiftedAlphabet[alphabet.indexOf(letter)] ?? letter)
				.join('');
		});
	}

	private getCleanKeyword(keyword: string): string[] {
		const cleanKeyword = keyword.toLowerCase().split('');
		// validate that the keyword has unique letters
		if (cleanKeyword.length !== [...new Set(cleanKeyword)].length) {
			throw new ArgumentError('The keyword must have unique letters.');
		}
		return cleanKeyword;
	}

	public getShiftedAlphabet(parameters: Required<Parameters>) {
		const keyword = this.getCleanKeyword(parameters.keyword);
		const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
		return [...keyword, ...alphabet.filter((char) => !keyword.includes(char))];
	}
}

export default AlphabetShift;
