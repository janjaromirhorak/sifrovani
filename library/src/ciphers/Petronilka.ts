import {Cipher} from "../core";
import {removeDiacritics} from "../core/utils";
import {CipherFunctionType} from "../core/Cipher";

export type Parameters = {}

const cipherFunction: CipherFunctionType<Parameters> = (cleartext) => {
    const petronilka = "PETRONILKA".split("");

    return removeDiacritics(cleartext).split("").map(char => {
        const index = petronilka.indexOf(char.toLocaleUpperCase());
        if (index >= 0) {
            return ((index + 1) % 10).toString();
        }
        return char;
    }).join("");
};

class Petronilka extends Cipher<Parameters> {
    constructor() {
        super({}, cipherFunction);
    }
}

export default Petronilka;