import {MorseSymbol, parameterDefinitions, Parameters} from "./MorseCode";
import SymbolDirectory from "./SymbolDirectory";

export type CharInfo = {
    symbols: string,
    character: string | undefined
}

export type SpiderRowsData = {
    left: CharInfo[][],
    right: CharInfo[][]
}

class Spider {
    private readonly parameters: Parameters;
    private readonly directory: SymbolDirectory;

    constructor(parameters: Parameters) {
        this.parameters = {
            ...parameters,
            // the spider can not be rendered including all the special symbol,
            // otherwise the table becomes too large and cluttered
            useSpecialSymbols: false
        };
        this.directory = new SymbolDirectory(this.parameters);
    }

    private numberToPath(num: number, padding: number): MorseSymbol[] {
        return num
            .toString(2)
            .padStart(padding, "0")
            .split("")
            .map(char => char === "0" ? MorseSymbol.DOT : MorseSymbol.DASH);
    }

    private symbolsToString(symbols: MorseSymbol[]): string {
        // @ts-ignore - the type inference is wrong here
        const dotSymbol: string = this.parameters.dotSymbol ?? parameterDefinitions.dotSymbol.defaultValue;
        // @ts-ignore - the type inference is wrong here
        const dashSymbol: string = this.parameters.dashSymbol ?? parameterDefinitions.dashSymbol.defaultValue;

        return symbols
            .map(symbol => symbol === MorseSymbol.DOT ? dotSymbol : dashSymbol)
            .join("");
    }

    private getRow(rowIndex: number): CharInfo[] {
        const length = Math.pow(2, rowIndex);

        let row: CharInfo[] = [];
        for (let i = 0; i < length; i++) {
            const path = this.numberToPath(i, rowIndex);
            row.push({
                symbols: this.symbolsToString(path),
                character: this.directory.getCharacter(path)
            });
        }

        return row;
    }

    public getRowsData(): SpiderRowsData {
        let completeRows: CharInfo[][] = [];

        let rowNumber = 1;
        while (true) {
            const currentRow = this.getRow(rowNumber);

            if (currentRow.every(item => item.character === undefined)) {
                break;
            }

            completeRows.push(currentRow);

            rowNumber++;
        }

        return completeRows.reduce((carry: SpiderRowsData, row) => {
            const {left, right} = carry;

            return {
                left: [...left, row.slice(0, row.length / 2)],
                right: [...right, row.slice(-(row.length / 2))]
            };
        }, {left: [], right: []})
    }
}

export default Spider;