import Cipher, {ParameterDefinitions, CipherFunctionType} from "../../core/Cipher";
import {removeDiacritics, splitToArrayWithCh} from "../../core/utils";

export type Parameters = {
    dotSymbol?: string,
    dashSymbol?: string,
    separatorSymbol?: string,
    useCh?: boolean,
    useNumbers?: boolean,
    useSpecialSymbols?: boolean,
    removeDiacritics?: boolean,
    removeUnnecessaryPunctuation?: boolean
};

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
    dotSymbol: {
        name: "Symbol for the dot",
        description: "A string to characterize the the dot in the Morse code.",
        defaultValue: "."
    },
    dashSymbol: {
        name: "Symbol for the dash",
        description: "A string to characterize the dash in the Morse code.",
        defaultValue: "-"
    },
    separatorSymbol: {
        name: "Symbol used to separate letters, words and sentences",
        description: "A string to characterize the separator",
        defaultValue: "|"
    },
    useCh: {
        name: "Use CH denotation",
        description: "Whether to use the special '----' denotation for the czech CH phone. If false, CH will be encoded as C and H.",
        defaultValue: true
    },
    useNumbers: {
        name: "Use number denotations",
        description: "Whether to use the number denotations. If false, the numbers will not be encoded.",
        defaultValue: true
    },
    useSpecialSymbols: {
        name: "Use special symbols",
        description: "Whether to use special symbols. If true, symbols such as ., ? or @ will be encoded as well. If false, the sentence-ending punctuation will be encoded using the sentence separator symbol.",
        defaultValue: false
    },
    removeDiacritics: {
        name: "Remove diacritics",
        description: "Whether to remove diacritics",
        defaultValue: true
    },
    removeUnnecessaryPunctuation: {
        name: "Remove unnecessary punctuation",
        description: "Whether to remove unnecessary punctuation, such as , or ;",
        defaultValue: true
    }
};

export enum MorseSymbol {
    DOT,
    DASH
}

class MorseCode extends Cipher<Parameters> {
    private basicDictionary: Map<string, MorseSymbol[]>;
    private numberDictionary: Map<string, MorseSymbol[]>;
    private specialSymbolDictionary: Map<string, MorseSymbol[]>;
    private sentenceEndingPunctuation: Set<string>;
    private unnecessaryPunctuation: Set<string>;
    private readonly morseCipherFunction: CipherFunctionType<Parameters>;

    constructor() {
        super(parameterDefinitions, (cleartext, parameters) => {
            return cleartext
                .split("\n")
                .map(row => row.length ? this.morseCipherFunction(row, parameters) : "")
                .join("\n");
        });

        this.basicDictionary = new Map<string, MorseSymbol[]>();
        this.basicDictionary.set("a", [MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("b", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("c", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("d", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("e", [MorseSymbol.DOT]);
        this.basicDictionary.set("f", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("g", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("h", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("ch", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("i", [MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("j", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("k", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("l", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("m", [MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("n", [MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("o", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("p", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("q", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("r", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.basicDictionary.set("s", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.basicDictionary.set("t", [MorseSymbol.DASH]);
        this.basicDictionary.set("u", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("v", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("w", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("x", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.basicDictionary.set("y", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.basicDictionary.set("z", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);

        this.numberDictionary = new Map<string, MorseSymbol[]>();
        this.numberDictionary.set("0", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.numberDictionary.set("1", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.numberDictionary.set("2", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.numberDictionary.set("3", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.numberDictionary.set("4", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.numberDictionary.set("5", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.numberDictionary.set("6", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.numberDictionary.set("7", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.numberDictionary.set("8", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.numberDictionary.set("9", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT]);

        this.specialSymbolDictionary = new Map<string, MorseSymbol[]>();
        this.specialSymbolDictionary.set(".", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set(",", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("?", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("'", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("!", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("/", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("(", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set(")", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("&", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set(":", [MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set(";", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("=", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("+", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("-", [MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("_", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("\"", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);
        this.specialSymbolDictionary.set("$", [MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DOT, MorseSymbol.DASH]);
        this.specialSymbolDictionary.set("@", [MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DASH, MorseSymbol.DOT, MorseSymbol.DASH, MorseSymbol.DOT]);

        this.sentenceEndingPunctuation = new Set<string>([".", "!", "?", ":"]);
        this.unnecessaryPunctuation = new Set<string>([",", ";", "-"]);

        this.morseCipherFunction = ((cleartext, parameters) => {
            const morseSymbolToCharacter = (symbol: MorseSymbol): string => {
                if (symbol === MorseSymbol.DOT) {
                    return parameters.dotSymbol;
                }

                return parameters.dashSymbol;
            };

            const transformClassicChar = (char: string): string => {
                const basic = this.basicDictionary.get(char);
                if (basic) {
                    return basic.map(morseSymbolToCharacter).join('');
                }

                if (parameters.useNumbers) {
                    const number = this.numberDictionary.get(char);
                    if (number) {
                        return number.map(morseSymbolToCharacter).join('');
                    }
                }

                if (parameters.useSpecialSymbols) {
                    const specialSymbol = this.specialSymbolDictionary.get(char);
                    if (specialSymbol) {
                        return specialSymbol.map(morseSymbolToCharacter).join('');
                    }
                }

                return char;
            };

            const transformSymbol = (symbol: string): string => {
                if (symbol === " ") {
                    return parameters.separatorSymbol;
                } else {
                    if (!parameters.useSpecialSymbols && this.sentenceEndingPunctuation.has(symbol)) {
                        return parameters.separatorSymbol;
                    } else {
                        return `${transformClassicChar(symbol)}${parameters.separatorSymbol}`
                    }
                }
            };

            let chars = parameters.useCh ? splitToArrayWithCh(cleartext) : [...cleartext];
            if (parameters.removeDiacritics) {
                chars = chars.map(removeDiacritics);
            }
            if (parameters.removeUnnecessaryPunctuation) {
                chars = chars.filter(char => !this.unnecessaryPunctuation.has(char));
            }

            chars = chars.map(char => char.toLocaleLowerCase());
            return chars.map(transformSymbol).join('') + parameters.separatorSymbol;
        });
    }

    public getSymbolMapsForConfig(config: Parameters): Map<string, MorseSymbol[]>[] {
        let maps: Map<string, MorseSymbol[]>[] = [];

        const basicSymbols = new Map<string, MorseSymbol[]>();
        for (const [key, value] of this.basicDictionary.entries()) {
            if (config.useCh || key !== "ch") {
                basicSymbols.set(key, value);
            }
        }
        maps.push(basicSymbols);

        if (config.useNumbers) {
            const numbers = new Map<string, MorseSymbol[]>();
            for (const [key, value] of this.numberDictionary.entries()) {
                numbers.set(key, value);
            }
            maps.push(numbers);
        }

        if (config.useSpecialSymbols) {
            const specialSymbols = new Map<string, MorseSymbol[]>();
            for (const [key, value] of this.specialSymbolDictionary.entries()) {
                specialSymbols.set(key, value);
            }
            maps.push(specialSymbols);
        }

        return maps;
    }

    public getSymbolMapForConfig(config: Parameters): Map<string, MorseSymbol[]> {
        const allSymbols = new Map<string, MorseSymbol[]>();

        for (const [key, value] of this.basicDictionary.entries()) {
            if (config.useCh || key !== "ch") {
                allSymbols.set(key, value);
            }
        }

        if (config.useNumbers) {
            for (const [key, value] of this.numberDictionary.entries()) {
                allSymbols.set(key, value);
            }
        }

        if (config.useSpecialSymbols) {
            for (const [key, value] of this.specialSymbolDictionary.entries()) {
                allSymbols.set(key, value);
            }
        }

        return allSymbols;
    }
}

export default MorseCode;