import MorseCode, {MorseSymbol, Parameters} from "./MorseCode";

class SymbolDirectory {
    private symbols: Map<string, string>;

    constructor(parameters: Parameters) {
        const cipher = new MorseCode();
        const characterMap = cipher.getSymbolMapForConfig(parameters);

        this.symbols = new Map<string, string>();

        for (const [character, symbols] of characterMap) {
            this.symbols.set(this.symbolsToKey(symbols), character);
        }
    }

    public getCharacter(symbols: MorseSymbol[]): string | undefined {
        return this.symbols.get(this.symbolsToKey(symbols));
    }

    private symbolsToKey(symbols: MorseSymbol[]): string {
        return symbols
            .map(symbol => symbol === MorseSymbol.DOT ? "." : "-")
            .join("");
    }
}

export default SymbolDirectory;