import {Cipher} from "../core";
import {ParameterDefinitions} from "../core/Cipher";
import {pad, splitToChunks, unzip, zip} from "../core/utils";

export type Parameters = {
    blockSize: number,
    capitalize?: boolean,
    paddingChars?: string[]
}

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
    blockSize: {
        name: "Block size",
        description: "Specifies the number of characters in a single block",
        defaultValue: 4
    },
    capitalize: {
        name: "Capitalize",
        description: "If true, all characters will be converted to uppercase.",
        defaultValue: false
    },
    paddingChars: {
        name: "Padding characters",
        description: "List of characters used for padding. Characters will be selected randomly from this list.",
        defaultValue: ["x"]
    }
};

class VpredFirst extends Cipher<Parameters> {
    constructor() {
        super(parameterDefinitions, (cleartext, parameters) => {
            // split to chars and unzip into two parts
            const parts = unzip(cleartext.replace(/\s/g, "").split(""));

            // find the closest multiple of blockSize that fits (the bigger of) the parts
            const length = Math.ceil(Math.max(...parts.map(part => part.length)) / parameters.blockSize) * parameters.blockSize;

            const [a, b] = parts
                // pad the parts to the desired length with characters from the paddingChars parameter
                .map(part => pad(part, parameters.paddingChars, length))
                // split each part to chunks of blockSize
                .map(part => splitToChunks(part, parameters.blockSize))
            ;

            // zip the parts back together
            const zipped = zip(a, b);

            // join everything to a string
            let result = zipped.map(chunk => chunk.join("")).join("");
            if (parameters.capitalize) {
                return result.toLocaleUpperCase();
            }
            return result;
        });
    }
}

export default VpredFirst;