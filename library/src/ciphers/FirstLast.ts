import { Cipher } from '../core';
import { removeWhiteSpaces, unzip } from '../core/utils';
import type { CipherFunctionType } from '../core/Cipher';

export type Parameters = {};

const cipherFunction: CipherFunctionType<Parameters> = (cleartext) => {
	const cleantext = removeWhiteSpaces(cleartext);
	const [first, second] = unzip(cleantext.split(''));
	return `${first.join('')}${second.reverse().join('')}`;
};

class FirstLast extends Cipher<Parameters> {
	constructor() {
		super({}, cipherFunction);
	}
}

export default FirstLast;
