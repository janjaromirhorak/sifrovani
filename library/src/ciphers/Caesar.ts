import { Cipher } from '../core';
import { ParameterDefinitions } from '../core/Cipher';
import { removeDiacritics } from '../core/utils';

export type Parameters = {
	shift: number;
};

export const parameterDefinitions: ParameterDefinitions<Parameters> = {
	shift: {
		name: 'Shift',
		description: 'The alphabetical distance between the source and the target alphabet',
		defaultValue: 0
	}
};

class Caesar extends Cipher<Parameters> {
	private readonly alphabet: string[];

	constructor() {
		super(parameterDefinitions, (cleartext, parameters) => {
			return removeDiacritics(cleartext)
				.split('')
				.map((letter) => this.shiftLetter(letter, parameters.shift))
				.join('');
		});

		this.alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
	}

	private simplifyAlphabetLocation(location: number): number {
		return ((location % this.alphabet.length) + this.alphabet.length) % this.alphabet.length;
	}

	private shiftLetter(letter: string, shift: number): string {
		const isUppercase: boolean = letter === letter.toLocaleUpperCase();
		const lowercaseLetter: string = letter.toLocaleLowerCase();

		const location = this.alphabet.findIndex(
			(alphabetLetter) => alphabetLetter === lowercaseLetter
		);
		if (location >= 0) {
			const shiftedLetter = this.alphabet[this.simplifyAlphabetLocation(location + shift)];
			if (isUppercase) {
				return shiftedLetter.toLocaleUpperCase();
			}
			return shiftedLetter;
		}

		return letter;
	}
}

export default Caesar;
