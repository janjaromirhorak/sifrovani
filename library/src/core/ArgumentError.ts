import CipherError from "./CipherError";

class ArgumentError extends CipherError {
}

export default ArgumentError;