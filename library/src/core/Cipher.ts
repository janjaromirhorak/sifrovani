type TypeOrUndefined<Type> = Type | undefined;

type ParameterDefinitionItem<ParameterValueType> = {
    readonly name: string,
    readonly description: string,
    defaultValue: ParameterValueType extends undefined ? NonNullable<ParameterValueType> : TypeOrUndefined<ParameterValueType>
}

type NotRequiredParameterDefinitions<ParameterObjectType> = {
    readonly [Property in keyof ParameterObjectType]: ParameterDefinitionItem<ParameterObjectType[Property]>;
}

export type ParameterDefinitions<ParameterObjectType> = Required<NotRequiredParameterDefinitions<ParameterObjectType>>;

export type CipherFunctionType<ParameterObjectType> = { (cleartext: string, parameters: Required<ParameterObjectType>): string };

class Cipher<ParameterObjectType> {
    private readonly parameterDefinitions: ParameterDefinitions<ParameterObjectType>;
    private readonly cipherFunction: CipherFunctionType<ParameterObjectType>;

    constructor(parameterDefinitions: ParameterDefinitions<ParameterObjectType>, cipherFunction: CipherFunctionType<ParameterObjectType>) {
        this.parameterDefinitions = parameterDefinitions;
        this.cipherFunction = cipherFunction;
    }

    getParameterDefinitions(): ParameterDefinitions<ParameterObjectType> {
        return this.parameterDefinitions;
    }

    private getDefaultParameterValues(): ParameterObjectType {
        let values = {};
        for (const key of Object.keys(this.parameterDefinitions)) {
            // @ts-ignore
            values[key] = this.parameterDefinitions[key].defaultValue;
        }
        // @ts-ignore
        return values;
    }

    run(cleartext: string, values: ParameterObjectType | undefined = undefined): string {
        // @ts-ignore
        const valuesWithDefaults: Required<ParameterObjectType> = {
            ...this.getDefaultParameterValues(),
            ...(values ?? {})
        };
        return this.cipherFunction(cleartext, valuesWithDefaults);
    }
}

export default Cipher;