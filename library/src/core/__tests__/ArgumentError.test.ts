import {expect} from 'chai';
import ArgumentError from "../ArgumentError";

describe("core/ArgumentError", () => {
    describe("constructor", () => {
        it("should have the correct name", () => {
            const error = new ArgumentError();
            expect(error.name).to.equal("ArgumentError");
        });

        it("should have a configurable message", () => {
            const error = new ArgumentError("custom message");
            expect(error.message).to.equal("custom message");
        });

        it("should throw the correct object, when thrown", () => {
            expect((() => {
                throw new ArgumentError()
            })).to.throw(ArgumentError);
        })
    })
});
