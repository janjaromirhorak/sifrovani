import {expect} from 'chai';
import Cipher, {ParameterDefinitions} from "../Cipher";

describe("core/Parameter", () => {
    describe("run", () => {
        it("should return cleartext", () => {
            const cipher = new Cipher<{}>({}, text => text);
            expect(cipher.run("this is cleartext")).to.equal("this is cleartext");
        });
    });

    describe("getParameterDefinitions", () => {
        it("should return parameter definitions", () => {
            type Parameters = {
                parameter?: number
            };

            const parameterDefinitions: ParameterDefinitions<Parameters> = {
                parameter: {
                    name: "Parameter",
                    description: "Description of the parameter",
                    defaultValue: 10
                }
            };

            const cipher = new Cipher<Parameters>(parameterDefinitions, text => text);
            expect(cipher.getParameterDefinitions()).to.eql(parameterDefinitions);
        });
    });

    describe("run behavior", () => {
        it("should use a value from the parameter value, if it exists", () => {
            type Parameters = {
                parameter?: number
            };

            const parameterDefinitions: ParameterDefinitions<Parameters> = {
                parameter: {
                    name: "Parameter",
                    description: "Description of the parameter",
                    defaultValue: 10
                }
            };

            const cipher = new Cipher<Parameters>(parameterDefinitions, (text, parameters) => `${text}${parameters.parameter}`);
            expect(cipher.run("test", {
                parameter: 15
            })).to.equal("test15");
        });

        it("should use a default value, if the parameter has not been provided", () => {
            type Parameters = {
                parameter?: number
            };

            const parameterDefinitions: ParameterDefinitions<Parameters> = {
                parameter: {
                    name: "Parameter",
                    description: "Description of the parameter",
                    defaultValue: 10
                }
            };

            const cipher = new Cipher<Parameters>(parameterDefinitions, (text, parameters) => `${text}${parameters.parameter}`);
            expect(cipher.run("test", {})).to.equal("test10");
        });
    })
});
