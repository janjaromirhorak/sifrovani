import {expect} from 'chai';
import * as Utils from '../utils';

describe("core/utils", () => {
    describe("removeWhiteSpaces", () => {
        it("should remove all white spaces from a string", () => {
            expect(Utils.removeWhiteSpaces(" this String\n\t Contains White\t Sp\races \t\t\t")).to.equal("thisStringContainsWhiteSpaces");
        });
    });

    describe("removeDiacritics", () => {
        it("should remove diacritics from a string", () => {
            expect(Utils.removeDiacritics("abcdefghijklmnopqrstuvwxyz \t")).to.equal("abcdefghijklmnopqrstuvwxyz \t");
            expect(Utils.removeDiacritics("ěščřžýáíéúůďťňó")).to.equal("escrzyaieuudtno");
            expect(Utils.removeDiacritics("ĚŠČŘŽÝÁÍÉÚŮĎŤŇÓ")).to.equal("ESCRZYAIEUUDTNO");
        });
    });

    describe("removeNonAsciiCharacters", () => {
        it("should non-ACII characters from a string", () => {
            expect(Utils.removeNonAsciiCharacters("abcdž1234")).to.equal("abcd1234");
        });
    });

    describe("removeNonAlphaNumericCharacters", () => {
        it("without explicitely specifying the allowSpaces argument, it should default to allowSpaces=true", () => {
            expect(Utils.removeNonAlphaNumericCharacters(" a b ěcščd1řžýáí2éúůď3ťňó4")).to.equal(" a b cd1234");
        });

        it("with allowSpaces=true, should remove non-alphanumeric characters, except spaces", () => {
            expect(Utils.removeNonAlphaNumericCharacters(" a b ěcščd1řžýáí2éúůď3ťňó4", true)).to.equal(" a b cd1234");
        });

        it("with allowSpaces=false, should remove non-alphanumeric characters, including spaces", () => {
            expect(Utils.removeNonAlphaNumericCharacters(" a b ěcščd1řžýáí2éúůď3ťňó4", false)).to.equal("abcd1234");
        });
    });

    describe("getRandomInt", () => {
        it("should return a number between the first and the second parameter", () => {
            expect(Utils.getRandomInt(15, 15)).to.equal(15);
            expect(Utils.getRandomInt(-2, 2)).to.be.within(-2, 2);
            expect(Utils.getRandomInt(5, 0)).to.be.within(0, 5);
        });
    });

    describe("arraysAreSame", () => {
        it("should return true, if the arrays are identical", () => {
            expect(Utils.arraysAreSame([], [])).to.equal(true);
            expect(Utils.arraysAreSame([1, 2, 3, 4, "eee", "f", -5], [1, 2, 3, 4, "eee", "f", -5])).to.equal(true);
        });

        it("should return true, if the arrays are not identical", () => {
            expect(Utils.arraysAreSame([], ["a"])).to.equal(false);
            expect(Utils.arraysAreSame([15], [])).to.equal(false);
            expect(Utils.arraysAreSame([15], ["15"])).to.equal(false);
            expect(Utils.arraysAreSame([15], [1])).to.equal(false);
            expect(Utils.arraysAreSame([1, 2, 3, 4, "eee", "f", -5], [1, 2, 3, 4, "eei", "f", -5])).to.equal(false);
            expect(Utils.arraysAreSame([1, 2, 3, 4, "eee", "f", -5], [1, 2, 3, 4, "eee", "f"])).to.equal(false);
        });
    });

    describe("splitToArrayWithCh", () => {
        it("should split an alphabet without CH correctly", () => {
            expect(Utils.splitToArrayWithCh("abcdefghijklmnopqrstuvwxyz")).to.be.eql(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]);
        });

        it("should split an alphabet with CH correctly", () => {
            expect(Utils.splitToArrayWithCh("abcdefghchijklmnopqrstuvwxyz")).to.be.eql(["a", "b", "c", "d", "e", "f", "g", "h", "ch", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]);
        });

        it("should preserve characted case", () => {
            expect(Utils.splitToArrayWithCh("aAbCcHChefchCH")).to.be.eql(["a", "A", "b", "C", "cH", "Ch", "e", "f", "ch", "CH"]);
        });
    })

    describe("unzip", () => {
        it("should correctly unzip an array", () => {
            expect(Utils.unzip(["a", "b", "c", "d"])).to.be.eql([["a", "c"], ["b", "d"]])
        })
    })

    describe("zip", () => {
        it("should correctly zip two arrays together", () => {
            expect(Utils.zip(["a", "b"], ["c", "d"])).to.be.eql(["a", "c", "b", "d"])
        })

        it("should throw an error, if the arrays are not of the same length", () => {
            expect(() => Utils.zip(["a", "b"], ["c"])).to.throw("a and b must be the same length")
        })
    })

    describe("pad", () => {
        it("should correctly pad an array", () => {
            expect(Utils.pad(["a", "b", "c"], ["x"], 5)).to.be.eql(["a", "b", "c", "x", "x"])
        })

        it("should throw an error, if the padSource array is empty", () => {
            expect(() => Utils.pad(["a", "b", "c"], [], 5)).to.throw("padSource must not be empty")
        })
    })

    describe("splitToChunks", () => {
        it("should correctly split an array into chunks", () => {
            expect(Utils.splitToChunks(["a", "b", "c", "d", "e"], 2)).to.be.eql([["a", "b"], ["c", "d"], ["e"]])
        })

        it("should throw an error, if the chunkSize parameter is below 1", () => {
            expect(() => Utils.splitToChunks(["a", "b", "c"], 0)).to.throw("chunkSize must be at least 1")
        })
    })
});