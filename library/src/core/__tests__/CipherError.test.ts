import {expect} from 'chai';
import CipherError from "../CipherError";

describe("core/CipherError", () => {
    describe("constructor", () => {
        it("should have the correct name", () => {
            const error = new CipherError();
            expect(error.name).to.equal("CipherError");
        });

        it("should have a configurable message", () => {
            const error = new CipherError("custom message");
            expect(error.message).to.equal("custom message");
        });

        it("should throw the correct object, when thrown", () => {
            expect((() => {
                throw new CipherError()
            })).to.throw(CipherError);
        })
    })
});
