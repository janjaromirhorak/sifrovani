export const removeWhiteSpaces = (text: string): string => {
    return text.replace(/\s/g, '');
};

export const removeDiacritics = (text: string): string => {
    // https://stackoverflow.com/a/37511463
    return text.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
};

export const removeNonAsciiCharacters = (text: string): string => {
    return text.replace(/[^\x00-\x7F]/g, "");
};

export const removeNonAlphaNumericCharacters = (text: string, allowSpaces = true): string => {
    if (allowSpaces) {
        return text.replace(/[^a-zA-Z0-9 ]/g, "");
    } else {
        return text.replace(/[^a-zA-Z0-9]/g, "");
    }
};

export const getRandomInt = (from: number, to: number): number => {
    if (to < from) {
        return getRandomInt(to, from);
    } else if (from === to) {
        return from;
    }

    if (from < 0) {
        return getRandomInt(0, to - from) + from;
    }

    return Math.round((Math.random() * (to - from)) - from);
};

export const arraysAreSame = (a: any[], b: any[]): boolean => {
    if (a.length !== b.length) {
        return false;
    }

    for (let i = 0 ; i < a.length ; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }

    return true;
};

export const splitToArrayWithCh = (text: string): string[] => {
    return [...text].reduce((array: string[], char: string): string[] => {
        if (array.length && char.toLowerCase() === "h") {
            const [lastChar] = array.slice(-1);
            if (lastChar.toLowerCase() === "c") {
                return [...array.slice(0, -1), `${lastChar}${char}`];
            } else {
                return [...array, char];
            }
        }
        return [...array, char];
    }, []);
};

export const unzip = <T>(input: T[]): [T[], T[]] => {
    return input.reduce(([first, second]: [T[], T[]], item: T, index): [T[], T[]] => {
        if (index % 2 === 0) {
            first.push(item);
        } else {
            second.push(item);
        }
        return [first, second];
    }, [[], []]);
}

export const zip = <T>(a: T[], b: T[]): T[] => {
    if (a.length !== b.length) {
        throw new Error("a and b must be the same length");
    }

    let result: T[] = [];

    while (b.length) {
        if (b.length > a.length) {
            result.push(...b.splice(0, 1));
        } else {
            result.push(...a.splice(0, 1))
        }
    }

    return result;
}

export const pad = <T>(arr: T[], padSource: T[], length: number): T[] => {
    if (padSource.length === 0) {
        throw new Error("padSource must not be empty");
    }

    while (arr.length < length) {
        arr.push(padSource[getRandomInt(0, padSource.length - 1)]);
    }
    return arr;
}

export const splitToChunks = <T>(arr: T[], chunkSize: number): T[][] => {
    if (chunkSize < 1) {
        throw new Error("chunkSize must be at least 1");
    }

    let chunks: T[][] = [];
    while (arr.length) {
        chunks.push(arr.splice(0, chunkSize));
    }
    return chunks;
}