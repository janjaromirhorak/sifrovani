export type Route = {
	path: string;
	id: string;
};

export const routes: Record<string, Route> = {
	homepage: {
		path: '/',
		id: '/'
	},
	abecedaSKlicem: {
		path: '/abeceda-s-klicem',
		id: '/abeceda-s-klicem'
	},
	britskaVlajka: {
		path: '/britska-vlajka',
		id: '/britska-vlajka'
	},
	druhaOddilova: {
		path: '/druha-oddilova',
		id: '/druha-oddilova'
	},
	morseovka: {
		path: '/morseovka',
		id: '/morseovka'
	},
	petronilka: {
		path: '/petronilka',
		id: '/petronilka'
	},
	posunutaAbecedaCaesar: {
		path: '/posunuta-abeceda-caesar',
		id: '/posunuta-abeceda-caesar'
	},
	prumeryPismenAcB: {
		path: '/prumery-pismen-ac-b',
		id: '/prumery-pismen-ac-b'
	},
	prvniOddilova: {
		path: '/prvni-oddilova',
		id: '/prvni-oddilova'
	},
	prvniPosledni: {
		path: '/prvni-posledni',
		id: '/prvni-posledni'
	},
	slovaPozpatku: {
		path: '/slova-pozpatku',
		id: '/slova-pozpatku'
	},
	souradnice: {
		path: '/souradnice',
		id: '/souradnice'
	},
	textPozpatku: {
		path: '/text-pozpatku',
		id: '/text-pozpatku'
	}
};
