import type { TransitionConfig } from 'svelte/transition';

type SlideXParams = {
	duration?: TransitionConfig['duration'];
	easing?: TransitionConfig['easing'];
};

export const slideX = (_: HTMLElement, config: SlideXParams): TransitionConfig => {
	return {
		...config,
		css: (t: number) => `transform: translateX(${100 * (t - 1)}%)`
	};
};
