import type { PageLoad } from './$types';

export const load = (() => ({ title: 'První oddílová' })) satisfies PageLoad;
