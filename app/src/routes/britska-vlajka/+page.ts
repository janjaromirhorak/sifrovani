import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Britská vlajka' })) satisfies PageLoad;
