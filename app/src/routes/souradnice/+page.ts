import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Souřadnice' })) satisfies PageLoad;
