import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Průměry písmen (AC = B)' })) satisfies PageLoad;
