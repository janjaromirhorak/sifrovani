import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Text pozpátku' })) satisfies PageLoad;
