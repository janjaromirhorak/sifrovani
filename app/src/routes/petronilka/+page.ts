import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Petronilka' })) satisfies PageLoad;
