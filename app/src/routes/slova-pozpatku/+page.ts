import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Slova pozpátku' })) satisfies PageLoad;
