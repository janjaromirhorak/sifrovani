import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Abeceda s klíčem' })) satisfies PageLoad;
