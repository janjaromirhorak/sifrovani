import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Druhá oddílová' })) satisfies PageLoad;
