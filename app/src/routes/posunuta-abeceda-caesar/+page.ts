import type { PageLoad } from './$types';

export const load = (() => ({ title: 'Posunutá abeceda (Caesar)' })) satisfies PageLoad;
