let {Cypher} = require('./Cypher');

const argv = require('yargs').argv;

let c = new Cypher();

// napoveda
if(argv.h) {
        console.log(`
--horizontal=VODOROVNY_KLIC
--vertical=SVISLY_KLIC
        `)
        return;
}


const souradnice = (text) => {
    // rozdelime text na pole znaku
    const charArray = Cypher.removeDiacritics(Cypher.toUpperCase(Cypher.filterUnimportantCharacters(text))).replace(' ', '').split('');

	const horizontalKey = argv.horizontal;
	const verticalKey = argv.vertical;

	if (!horizontalKey) {
		console.error("Zadej prosim vodorovny klic pomoci parametru --horizontal=klic");
		return;
	}

	if (!verticalKey) {
		console.error("Zadej prosim svisly klic pomoci parametru --vertical=klic");
		return;
	}

    const alphabet = 'ABCDEFGHIJKLMNOPRSTUVWXYZ'.split('');

	if (horizontalKey.length * verticalKey.length < alphabet.length) {
		console.error("Klice nejsou dost dlouhe, aby pokryly celou abecedu.");
		return;
	}

	let positionMap = new Map();
	alphabet.forEach((value, key) => {
		const rowIndex = Math.floor(key / horizontalKey.length);
		const colIndex = key % horizontalKey.length;
		positionMap.set(value, `${horizontalKey[rowIndex]}${verticalKey[colIndex]}`);
		console.log(value, `${horizontalKey[rowIndex]}${verticalKey[colIndex]}`)
	})

	return charArray.map(char => {
		return positionMap.get(char) ?? char;
	}).join(' ');
};

c.processInput(souradnice);