/**
 * velký polský kříž, místo obdélníčku je však zapsána trojice čísel X00, přičemž X je na pozici,
 * na které by byl v obdélníku puntík a X má číslo odpovídající pozici obdélníčku v tabulce.
 * A je tedy 100, B je 010, D je 200.
 *
 * ABC|DEF|GH(CH)
 * IJK|LMN|OPQ
 * RST|UVW|XYZ
 */

let {Cypher} = require('./Cypher');

let c = new Cypher();

const prumeryPismen = (text) => {
    const maxDistance = 1;
    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let substitutionMap = new Map();

    for (let idx = 0 ; idx < alphabet.length ; idx++) {
        const char = alphabet[idx];
        for (let i = 1 ; i <= maxDistance; i++) {
            const leftIndex = (idx + alphabet.length - i) % alphabet.length;
            const rightIndex = (idx + i) % alphabet.length;
            const leftChar = alphabet[leftIndex];
            const rightChar = alphabet[rightIndex];

            const charPair = Cypher.toUpperCase(leftChar + rightChar);

            if(substitutionMap.has(char)) {
                substitutionMap.set(char, [...substitutionMap.get(char), charPair]);
            } else {
                substitutionMap.set(char, [charPair]);
            }
        }
    }

    const cleanedText = Cypher.toLowerCase(Cypher.removeDiacritics(text.replace(/ /gi, '|'))).split('');

    return cleanedText.map(char => {
        if (substitutionMap.has(char)) {
            const variants = substitutionMap.get(char);
            return variants[Cypher.getRandomInt(0, variants.length - 1)];
        } else {
            return char;
        }
    }).join(' ');
};

c.processInput(prumeryPismen);

