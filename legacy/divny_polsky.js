/**
 * velký polský kříž, místo obdélníčku je však zapsána trojice čísel X00, přičemž X je na pozici,
 * na které by byl v obdélníku puntík a X má číslo odpovídající pozici obdélníčku v tabulce.
 * A je tedy 100, B je 010, D je 200.
 *
 * ABC|DEF|GH(CH)
 * IJK|LMN|OPQ
 * RST|UVW|XYZ
 */

let {Cypher} = require('./Cypher');

let c = new Cypher();

const divnyPolsky = (text) => {
    const charMap = new Map();

    charMap.set('a', '100');
    charMap.set('b', '010');
    charMap.set('c', '001');

    charMap.set('d', '200');
    charMap.set('e', '020');
    charMap.set('f', '002');

    charMap.set('g', '300');
    charMap.set('h', '030');
    charMap.set('ch', '003');

    charMap.set('i', '400');
    charMap.set('j', '040');
    charMap.set('k', '004');

    charMap.set('l', '500');
    charMap.set('m', '050');
    charMap.set('n', '005');

    charMap.set('o', '600');
    charMap.set('p', '060');
    charMap.set('q', '006');

    charMap.set('r', '700');
    charMap.set('s', '070');
    charMap.set('t', '007');

    charMap.set('u', '800');
    charMap.set('v', '080');
    charMap.set('w', '008');

    charMap.set('x', '900');
    charMap.set('y', '090');
    charMap.set('z', '009');

    const chars = Cypher.splitCh(
        Cypher.toLowerCase(
            Cypher.filterUnimportantCharacters(text)
        )
    ).map(char => Cypher.removeDiacritics(char));

    return chars.map(char => {
        if (charMap.has(char)) {
            return charMap.get(char);
        } else {
            return '„' + char + '“';
        }
    }).join(' ');

    return text;
};

c.processInput(divnyPolsky);

