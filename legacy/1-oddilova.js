let {Cypher} = require('./Cypher');

let c = new Cypher();

const prvniOddilova = (text) => {
    const blockWidth = 4;

    // rozdelime text na pole znaku
    const charArray = Cypher.toUpperCase(Cypher.filterUnimportantCharacters(text)).split('');

    // doplnime pole znaky x, dokud neni jeho delka delitelna dvojnasobkem sirky bloku
    while (charArray.length % (blockWidth * 2) > 0) {
        charArray.push('X');
    }

    // rozdelime na horni a dolni radek
    const upperLine = charArray.filter((char, index) => !(index % 2));
    const lowerLine = charArray.filter((char, index) => !!(index % 2));

    // spojime do bloku delky blockWidth
    const joinToBlocks = (arr) => arr.reduce((accumulator, char, index) => {
        if (index % blockWidth === 0) {
            accumulator.push(char);
        } else {
            accumulator[accumulator.length - 1] += char;
        }
        return accumulator;
    }, []);

    const upperBlocks = joinToBlocks(upperLine);
    const lowerBlocks = joinToBlocks(lowerLine);

    // slepime do vysledne sifry zipovanim horniho a dolniho bloku
    let result = '';

    for (let i = 0; i < upperBlocks.length; i++) {
        result += upperBlocks[i];
        result += lowerBlocks[i];
    }

    return result + "\n1," + blockWidth;
};

c.processInput(prvniOddilova);