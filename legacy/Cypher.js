class Cypher {
    constructor() {
        process.stdin.setEncoding('utf8');
    }

    processInput(transform = text => text) {
        let uncypheredText = '';

        process.stdin.on('readable', () => {
            let chunk;
            // Use a loop to make sure we read all available data.
            while ((chunk = process.stdin.read()) !== null) {
                uncypheredText += chunk;
            }
        });

        process.stdin.on('end', () => {
            // orizneme \n na konci
            if(uncypheredText.substr(uncypheredText.length - 1) === "\n") {
                uncypheredText = uncypheredText.substr(0, uncypheredText.length - 1);
            }
            console.log(transform(uncypheredText));
        });
    }

    static filterWhitespaces(text) {
        const whitespaces = new Set([" ", "\n", "\t"]);
        return text.split('').filter(char => {
            return !whitespaces.has(char);
        }).join('');
    }

    static filterUnimportantCharacters(text) {
        const unimportantCharacters = new Set([".", ",", "!", "?"]);
        return this.filterWhitespaces(text).split('').filter(char => {
            return !unimportantCharacters.has(char);
        }).join('');
    }

    static getAccentCaseMap(reversed = false) {
        let charMap = new Map();
        charMap.set('ě', 'Ě');
        charMap.set('š', 'Š');
        charMap.set('č', 'Č');
        charMap.set('ř', 'Ř');
        charMap.set('ž', 'Ž');
        charMap.set('ý', 'Ý');
        charMap.set('á', 'Á');
        charMap.set('í', 'Í');
        charMap.set('é', 'É');
        charMap.set('ú', 'Ú');
        charMap.set('ů', 'Ů');
        charMap.set('ť', 'Ť');
        charMap.set('ď', 'Ď');
        charMap.set('ň', 'Ň');
        charMap.set('ó', 'Ó');

        if(reversed) {
            let reversedMap = new Map();
            for (let pair of charMap) {
                reversedMap.set(pair[1], pair[0]);
            }
            return reversedMap;
        }

        return charMap;
    }

    static toUpperCase(text) {
        const charMap = this.getAccentCaseMap();

        return text.toUpperCase().split('').map(char => {
            return charMap.has(char) ? charMap.get(char) : char;
        }).join('');
    }

    static toLowerCase(text) {
        const charMap = this.getAccentCaseMap(true);

        return text.toLowerCase().split('').map(char => {
            return charMap.has(char) ? charMap.get(char) : char;
        }).join('');
    }

    /**
     * Rozdeli string na pismenka, ale ch je jedno pismenko
     * @param text
     */
    static splitCh(text) {
        let arr = text.split('');
        let joinedArr = [];
        while (arr.length > 0) {
            let char = arr.shift();
            if(char.toLowerCase() === 'c' && arr.length > 0 && arr[0].toLowerCase() === 'h') {
                char += arr.shift();
            }
            joinedArr.push(char);
        }
        return joinedArr;
    }

    static removeDiacritics(text) {
        // https://stackoverflow.com/a/37511463
        return text.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    }

    static getRandomInt(from, to) {
        if(to < from) {
            return this.getRandomInt(to, from);
        } else if (from === to) {
            return from;
        }

        const interval = to - from;
        return Math.round((Math.random() * interval) - from);
    }
}

module.exports = {
    Cypher
};