let {Cypher} = require('./Cypher');

const argv = require('yargs').argv;

let c = new Cypher();


const druhaOddilova = (text) => {
    // rozdelime text na pole znaku
    const charArray = Cypher.filterUnimportantCharacters(text).replace(' ', '').split('');

	let firstHalf = '';
	let secondHalf = '';

	charArray.forEach((char, key) => {
		if(key % 2) {
			secondHalf = char + secondHalf;
		} else {
			firstHalf += char;
		}
	});

	return firstHalf + secondHalf;
};


c.processInput(druhaOddilova);