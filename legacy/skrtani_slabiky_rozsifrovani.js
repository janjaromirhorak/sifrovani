/**
 * velký polský kříž, místo obdélníčku je však zapsána trojice čísel X00, přičemž X je na pozici,
 * na které by byl v obdélníku puntík a X má číslo odpovídající pozici obdélníčku v tabulce.
 * A je tedy 100, B je 010, D je 200.
 *
 * ABC|DEF|GH(CH)
 * IJK|LMN|OPQ
 * RST|UVW|XYZ
 */

let {Cypher} = require('./Cypher');

let c = new Cypher();

const skrtaniSlabikRozsifrovani = (text) => {
    return text.replace(/(.)don/g, '$1');
};

c.processInput(skrtaniSlabikRozsifrovani);

