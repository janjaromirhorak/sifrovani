let {Cypher} = require('./Cypher');

let c = new Cypher();

const argv = require('yargs').argv;

// napoveda
if(argv.h) {
	console.log(`
Posun o x doleva:
	-lx nebo --left=x

Posun o x doprava:
	-rx nebo --right=x
	`)
	return;
}

// -l prepne smer doleva
const directionLeft = !!(argv.l || argv.left);

let shiftAmount = 0;
if(directionLeft) {
	if (argv.l) {
		shiftAmount = argv.l;	
	} else if (argv.left) {
		shiftAmount = argv.left;
	}
} else {
	if (argv.r) {
		shiftAmount = argv.r;
	} else if (argv.right) {
		shiftAmount = argv.right;
	}
}
shiftAmount = Number(shiftAmount);

const showKey = !!(argv.k || argv.key)

const ocislovanaAbeceda = (text) => {
	// doprava - zaporny parametr, doleva - kladny
	const recievedParam = shiftAmount * (directionLeft ? 1 : -1);

    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
	
	let param = recievedParam;
	while (param < 0) {
		param += alphabet.length;
	}

    let alphabetMap = new Map();
    for (let i = 0 ; i < alphabet.length ; i++) {
        alphabetMap.set(alphabet[i], alphabet[(i + param) % alphabet.length]);
    }

	let result = Cypher.toLowerCase(Cypher.removeDiacritics(text)).split('').filter(char => char !== ',' && char !== '.').map(
        char => alphabetMap.has(char) ? alphabetMap.get(char) : char
    ).join('');

    if(showKey) {
    	result += "\n";
		result += `3${directionLeft ? "L" : "P"}${shiftAmount}`;
    }

    return result;
};

c.processInput(ocislovanaAbeceda);

