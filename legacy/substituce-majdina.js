#!/usr/bin/env node

var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

const charMap = new Map();

charMap.set("6", "C");
charMap.set("53", "I");
charMap.set("19", "K");
charMap.set("66", "Dy");
charMap.set("52", "Te");
charMap.set("75", "Re");
charMap.set("57", "La");
charMap.set("16", "S");
charMap.set("88", "Ra");
charMap.set("50", "Sn");
charMap.set("39", "Y");
charMap.set("10", "Ne");
charMap.set("1", "H");
charMap.set("71", "Lu");
charMap.set("92", "U");
charMap.set("58", "Ce");
charMap.set("73", "Ta");
charMap.set("85", "At");
charMap.set("110", "Ds");
charMap.set("59", "Pr");
charMap.set("8", "O");
charMap.set("3", "Li");

rl.on('line', function(line){
	let returnString = "";
    for (symbol of line.split(" ")) {
	   	if(charMap.has(symbol)) {
    		returnString += charMap.get(symbol) + " ";
    	} else {
    		returnString += symbol + " ";
    	}
    }
    console.log(returnString);
});
