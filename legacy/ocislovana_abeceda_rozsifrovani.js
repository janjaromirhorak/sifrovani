let {Cypher} = require('./Cypher');

let c = new Cypher();

const ocislovanaAbecedaRozsifrovani = (text) => {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let alphabetMap = new Map();
    for (let i = 0 ; i < alphabet.length ; i++) {
        alphabetMap.set(i + 1, alphabet[i]);
    }

    return text.split(';').map(item => {
        const number = Number(item);
        if(alphabetMap.has(number)) {
            return alphabetMap.get(number);
        }
        return item;
    }).join('');
};

c.processInput(ocislovanaAbecedaRozsifrovani);

