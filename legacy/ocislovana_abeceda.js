let {Cypher} = require('./Cypher');

let c = new Cypher();

const ocislovanaAbeceda = (text) => {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let alphabetMap = new Map();
    for (let i = 0 ; i < alphabet.length ; i++) {
        alphabetMap.set(alphabet[i], i + 1);
    }

    return Cypher.toLowerCase(Cypher.removeDiacritics(text)).split('').filter(char => char !== ',' && char !== '.').map(
        char => alphabetMap.has(char) ? alphabetMap.get(char) : char
    ).join(';');
};

c.processInput(ocislovanaAbeceda);

