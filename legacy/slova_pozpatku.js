let {Cypher} = require('./Cypher');

let c = new Cypher();

const slovaPozpatku = (text) => {
    return text.split(' ').map(word => word.split('').reverse().join('')).join(' ');
};

c.processInput(slovaPozpatku);

