#!/usr/bin/env node

var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

const charMap = new Map();

charMap.set("a", "l");
charMap.set("b", "s");
charMap.set("c", "e");
charMap.set("d", "a");
charMap.set("e", "o");
charMap.set("f", "q");
charMap.set("g", "v");
charMap.set("h", "n");
charMap.set("i", "c");
charMap.set("j", "g");
charMap.set("k", "t");
charMap.set("l", "m");
charMap.set("m", "z");
charMap.set("n", "f");
charMap.set("o", "k");
charMap.set("p", "d");
charMap.set("q", "y");
charMap.set("r", "x");
charMap.set("s", "h");
charMap.set("t", "j");
charMap.set("u", "r");
charMap.set("v", "w");
charMap.set("w", "b");
charMap.set("x", "i");
charMap.set("y", "p");
charMap.set("z", "u");

charMapReverse = new Map();

for (let [key, value] of charMap) {
	charMapReverse.set(value, key);
}

rl.on('line', function(line){
	let returnString = "";
    for (char of line) {
	   	if(charMapReverse.has(char)) {
    		returnString += charMapReverse.get(char);
    	} else {
    		returnString += char;
    	}
    }
    console.log(returnString);
})