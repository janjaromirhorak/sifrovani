let {Cypher} = require('./Cypher');

const argv = require('yargs').argv;

let c = new Cypher();


const druhaOddilova = (text) => {
    // rozdelime text na pole znaku
    const charArray = Cypher.toUpperCase(Cypher.filterUnimportantCharacters(text)).replace(' ', '').split('');

	const squareSize = Math.ceil(Math.sqrt(charArray.length));

	let rowPointer = 0;
	let colPointer = -1;
	let step = 0;

	let finalTable = Array(squareSize * squareSize).fill("X");
	let visitedLocations = new Set();

	for (const char of charArray) {
		if (step === 0) {
			colPointer++;
			
			if (visitedLocations.has(rowPointer * squareSize + colPointer)) {
				rowPointer++;
				colPointer = rowPointer;
			}
		}

		visitedLocations.add(rowPointer * squareSize + colPointer);
	
		finalTable[rowPointer * squareSize + colPointer] = char;

		[rowPointer, colPointer] = [colPointer, squareSize - 1 - rowPointer];

		step = (step + 1) % 4;
	}

	return `${finalTable.join('')}
2`;
};

c.processInput(druhaOddilova);